import React from 'react';
import styles from './FirstCheckControlComponent.module.scss';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { GameAction, GameSelector } from '../../store/game/gameSlice';

const CheckControlComponent = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const deck = useAppSelector(GameSelector.deck);
  const gamblers = useAppSelector(GameSelector.gamblers);
  const player = gamblers[0];
  const playerPoint = player.cards.reduce((sum, card) => sum + card.point, 0) % 10;
  const drawCardGamblers = gamblers.filter((gambler, gamblerIndex) => gamblerIndex !== 0
    && gambler.cards.reduce((sum, card) => sum + card.point, 0) % 10 <= 4);

  const handleDrawCardClick = () => {
    const card = deck[0];
    new Promise<void>(resolve => setTimeout(() => {
      dispatch(GameAction.drawCard({ gamblerIndex: 0, card: card }));
      resolve();
    }, 300));
    handleNextClick();
  };

  const handleNextClick = () => {
    const cards = deck.slice(0, drawCardGamblers.length);
    cards.map((card, index) => {
      const gamblerIndex = gamblers.findIndex(g => g.name === drawCardGamblers[index].name);
      new Promise<void>(resolve => setTimeout(() => {
        dispatch(GameAction.drawCard({ gamblerIndex: gamblerIndex, card: card }));
        resolve();
      }, 300 * (index + 1)));
    });

    dispatch(GameAction.changeScene('DRAW'));
  };

  return playerPoint >= 8
    ? (<div className="d-grid gap-2">
        <button
          className="btn btn-secondary"
          onClick={handleNextClick}
        >
          ไม่จั่ว
        </button>
      </div>)
    : (<div className="row">
      <div className="col-6 d-grid gap-2">
        <button
          className="btn btn-primary"
          onClick={handleDrawCardClick}
        >
          จั่วไพ่
        </button>
      </div>
      <div className="col-6 d-grid gap-2">
        <button
          className="btn btn-secondary"
          onClick={handleNextClick}
        >
          ไม่จั่ว
        </button>
      </div>
    </div>);
};

export default CheckControlComponent;
