import React from 'react';
import styles from './BetControlComponent.module.scss';
import { useAppSelector } from '../../hooks/redux';
import { GameSelector } from '../../store/game/gameSlice';
import type { Ref } from '../modal/ref';
import BetModalComponent from './BetModalComponent';

const BetControlComponent = (): JSX.Element => {
  const betModal = React.useRef<Ref>(null);
  const gamblers = useAppSelector(GameSelector.gamblers);
  const player = gamblers.length > 0
    ? gamblers[0]
    : { name: '-', aka: '-', cards: [], coins: 0, bet: 0, criminals: [] };

  const handleBetClick = () => {
    betModal.current?.show();
  };

  return (<>
    <div className="d-grid gap-2">
      <button
        className="btn btn-primary"
        onClick={handleBetClick}
      >
        วางเดิมพัน
      </button>
    </div>

    {/* Modals */}
    <BetModalComponent
      ref={betModal}
      coins={player.coins}
    />
  </>);
};

export default BetControlComponent;
