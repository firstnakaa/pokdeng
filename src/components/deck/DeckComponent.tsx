import React from 'react';
import styles from './DeckComponent.module.scss';
import { useAppSelector } from '../../hooks/redux';
import { GameSelector } from '../../store/game/gameSlice';

const DeckComponent = (): JSX.Element => {
  const deck = useAppSelector(GameSelector.deck);
  const ref = React.useRef<HTMLDivElement>(null);
  const [span, setSpan] = React.useState(0);

  React.useLayoutEffect(() => {
    const handleResize = () => setSpan((ref.current!.offsetWidth - 86) / 51);
    window.addEventListener('resize', handleResize);
    handleResize();
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return (<>
    <div ref={ref} className={`${styles.deck} mb-3`}>
      {deck.map((_, index) =>
        <div
          key={index}
          className={styles.back}
          style={{position: 'absolute', top: 0, left: span * index}}
        ></div>
      )}
    </div>
    <div className="d-flex justify-content-center">{`ไพ่: ${deck.length}`}</div>
  </>);
};

export default DeckComponent;
