import React from 'react';
import type { RouteObject } from 'react-router-dom';
import LayoutComponent from '../components/layout/LayoutComponent';

const GamePage = React.lazy(() => import('../pages/game/GamePage'));
const NotFoundPage = React.lazy(() => import('../pages/not-found/NotFoundPage'));

const routes: RouteObject[] = [
  {
    path: '/',
    element: <LayoutComponent />,
    children: [
      { 
        index: true,
        element: <GamePage />,
      },
    ],
  },
  { 
    path: '*',
    element: <NotFoundPage />,
  },
];

export default routes;
