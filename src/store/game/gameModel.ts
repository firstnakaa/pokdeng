export type Scene = 'SLEEP' | 'START' | 'BET' | 'DEAL' | 'CHECK' | 'DRAW' | 'FINISH' | 'CRIME';
export type Suit = 'SPADE' | 'HEART' | 'DIAMOND' | 'CLUB';
export type Symbol = 'A' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | 'J' | 'Q' | 'K';
export type Point = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 0;

export interface Card {
  suit: Suit;
  symbol: Symbol;
  point: Point;
}

export interface OnHandCard extends Card {
  open: boolean;
}

export interface Gambler {
  name: string;
  aka: string;
  cards: OnHandCard[];
  coins: number;
  bet: number;
  criminals: Criminal[];
}

export interface Criminal {
  name: string;
  coins: number;
}
